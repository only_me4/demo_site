<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductRepository
{
    private $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function removeProduct($id)
    {
        $product = $this->model->where('id', $id)->firstOrFail()->delete();
    }

    public function getProductById($id)
    {
        return $this->model->where('id', $id)->firstOrFail();
    }

    public function storeProduct($rawData)
    {
        $product = new Product();
        $data = collect($rawData)->only($product->getFillable())->all();
        $product->fill($data);
        $this->model->create($data);
    }

    public function updateProductById($id, $name, $description, $price)
    {
        $updateData = ['id' => $id, 'name' => $name, 'description' => $description, 'price' => $price];
        $this->model->where('id', $id)->update($updateData);
    }

    public function hasProductWithId($id)
    {
        return $this->model->where('id', $id)->first() !== null;
    }
}
