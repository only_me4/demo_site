<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
    private $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function getCategories()
    {
        return $this->model->distinct('name')->get(['name']);
    }

    public function getProductsByCategoryId($id)
    {
        return $this->model->where('id', $id)->firstOrFail()->products()->paginate(6);
    }
}
