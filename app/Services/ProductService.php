<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Services\UploadFileService;
use Intervention\Image\Facades\Image as Image;

class ProductService
{
    private $repository;
    private $uploadFileService;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
        $this->uploadFileService = new UploadFileService();
    }

    public function removeProduct($id)
    {
        $this->repository->removeProduct($id);
    }

    public function getProductById($id)
    {
        return $this->repository->getProductById($id);
    }

    public function storeProduct($rawData)
    {
        $allowedImgTypes = ['image/jpeg','image/gif','image/png'];
        $imgType = $rawData['image']->getClientMimeType();

        $newImgPath = 'null.png';
        if (in_array($imgType, $allowedImgTypes)){
            $newImgPath = time() . '.' . $rawData['image']->extension();
            $rawData['image']->move(public_path('images'), $newImgPath);
        }

        $rawData['image_path'] = $newImgPath;
        $this->repository->storeProduct($rawData);
    }

    public function updateProductById($id, $name, $description, $price, $image)
    {
        if ($image !== null) {
            $newImgName = $this->uploadFileService->uploadImage($image);
            $updateData['image_path'] = $newImgName;
        }
        $this->repository->updateProductById($id, $name, $description, $price);
    }

    public function hasProductWithId($id)
    {
        return $this->repository->hasProductWithId($id);
    }
};
