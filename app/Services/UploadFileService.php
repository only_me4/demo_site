<?php

namespace App\Services;


class UploadFileService
{
    public function uploadImage($imageData)
    {
        $newImgName = time() . '.' . $imageData->extension();
        $imageData->move(public_path('images'), $newImgName);
        return $newImgName;
    }
};
