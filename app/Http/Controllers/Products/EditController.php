<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\ProductService;

class EditController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function __invoke($id)
    {
        $product = $this->service->getProductById($id);
        return view('edit')->with('product', $product);
    }
}
