<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function __invoke(Request $request, $id)
    {
        $name = $request->name;
        $description = $request->description;
        $price = $request->price;
        $image = $request->image;

        $this->service->updateProductById($id, $name, $description, $price, $image);
        return redirect('/');
    }
}
