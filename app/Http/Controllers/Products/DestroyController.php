<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;


class DestroyController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function __invoke($id)
    {
        if ($this->service->hasProductWithId($id))
            $this->service->removeProduct($id);
        return redirect('/');
    }
}
