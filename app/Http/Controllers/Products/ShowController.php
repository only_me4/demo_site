<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductService;

class ShowController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function __invoke($id)
    {
        $product = $this->service->getProductById($id);
        return view('show')->with('product', $product);
    }
}
