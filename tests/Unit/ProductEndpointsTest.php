<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductEndpointsTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    public function test_the_product_creater_page_returns_successfully()
    {
        $response = $this->get('/products/create');
        $response->assertStatus(200);
    }

    public function createExampleProduct($name = 'test product')
    {
        $product = \App\Models\Product::factory()->create(['name'=>$name]);

        $this->assertDatabaseHas('products',[
            'name' => $name
        ]);

        return $product;
    }

    public function test_specific_product_page_returns_successfully()
    {
        $product = $this->createExampleProduct();

        $response = $this->get('/products/'.$product->id);
        $response->assertStatus(200);
    }

    public function test_the_product_edit_page_returns_successfully()
    {
        $product = $this->createExampleProduct();

        $response = $this->get('/products/'.$product->id.'/edit');
        $response->assertStatus(200);
    }

    public function test_product_delete_page()
    {
        $product = $this->createExampleProduct();

        $response = $this->delete('/products/'.$product->id);
        $response->assertStatus(302);
    }
}
