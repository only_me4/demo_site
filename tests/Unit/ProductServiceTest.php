<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Product;

class ProductServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    private $productService;

    public function setUp(): void
    {
        parent::setUp();
        $this->productService = $this->app->make('App\Services\ProductService');
    }

    public function createExampleProduct($name = 'test product')
    {
        $product = \App\Models\Product::factory()->create(['name'=>$name]);

        $this->assertDatabaseHas('products',[
            'name' => $name
        ]);
    }

    public function test_create_product_service()
    {
        $this->createExampleProduct();

        $this->assertDatabaseHas('products',[
            'name' => 'test product'
        ]);
    }

    public function test_update_product_service()
    {
        $this->createExampleProduct('test product 2');

        $product = Product::where('name', 'test product 2')->firstOrFail();
        $this->productService->updateProductById($product->id,'test2updated',$product->description, $product->price, null);

        $this->assertDatabaseHas('products',[
            'name' => 'test2updated'
        ]);
    }

    public function test_delete_product_service()
    {
        $this->createExampleProduct('product to delete');

        $productId = Product::where('name', 'product to delete')->firstOrFail()->id;
        $this->productService->removeProduct($productId);

        $this->assertDatabaseMissing('products',[
            'name' => 'product to delete'
        ]);
    }
}
