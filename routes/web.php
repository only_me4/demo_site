<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
//use App\Http\Controllers\Products\IndexController;
//use App\Http\Controllers\Products\CreateController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::resource('/products', ProductsController::class);
Route::resource('/categories', CategoriesController::class);

//Index products
Route::get('/',  App\Http\Controllers\Products\IndexController::class);

//Create product
Route::get('/products/create', App\Http\Controllers\Products\CreateController::class);
Route::post('/products/', App\Http\Controllers\Products\StoreController::class); //store product after submit form

//Index specific product
Route::get('/products/{id}', App\Http\Controllers\Products\ShowController::class)
    ->where('id', '[0-9]+');

//Edit Product
Route::get('/products/{id}/edit', App\Http\Controllers\Products\EditController::class);
Route::put('/products/{id}', App\Http\Controllers\Products\UpdateController::class);

//Delete Product
Route::delete('/products/{id}', App\Http\Controllers\Products\DestroyController::class);
