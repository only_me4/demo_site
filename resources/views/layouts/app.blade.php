<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Products Manager</title>
    <link rel="stylesheet" href="https://bootswatch.com/5/minty/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/app.css') }}" />
</head>
<body style="background-color: #eee;">

{{--    navbar--}}
    <div id = 'app'  style = "min-height: 100vh">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="#" style = "margin-left: 1rem">Products Manager</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link
                    @if(Request::is('/'))
                        active
                    @endif
                    " href="/">Home</a>
                    <a class="nav-item nav-link
                    @if(Request::path() == 'products/create')
                        active
                    @endif
                    " href="/products/create">Add</a>
{{--                    <a class="nav-item nav-link--}}
{{--                    @if(Request::path() == 'edit')--}}
{{--                        active--}}
{{--                    @endif" href="/edit">Edit</a>--}}
                </div>
            </div>
        </nav>
        {{--        content--}}

        @yield('content')

{{--        footer--}}
        <footer class="bg-primary text-center text-lg-start">
            <!-- Copyright -->
            <div class="text-center text-white p-3">
                This is the footer
            </div>
            <!-- Copyright -->
        </footer>
    </div>

</body>
</html>
