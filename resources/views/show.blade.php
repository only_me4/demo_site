@extends('layouts.app')

@section('content')
    <section style="background-color: #eee;">
        <div class="text-center container-sm py-5">
            <h4 class="mt-4 mb-5"><strong>Products:</strong></h4>

            @forelse($product->categories as $category)
                {{$category}};
            @empty
                <p>This product doesn't have any categories</p>
            @endforelse
        </div>
    </section>
@endsection
