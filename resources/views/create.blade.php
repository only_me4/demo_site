@extends("layouts.app")

@section("content")
    <style>
        .container {
            max-width: 540px;
        }

        .form-group {
            margin: 10px;
        }
    </style>
    <section style="background-color: #eee;">
        <div class="container py-5">
            <form action="/products" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name-input">Name</label>
                    <input type="text" class="form-control" id="name-input" name="name"/>
                </div>
                <div class="form-group">
                    <label for="price-input">Price($)</label>
                    <input type="number" class="form-control" id="price-input" name="price"/>
                </div>

                <div class="form-group">
                    <label for="description-input">Description</label>
                    <textarea class="form-control" id="description-input" rows="3" name="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="image_input">Image</label><br/>
                    <input name="image" type="file" class="form-control-file" id="image_input"/>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-primary mt-2" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </section>
@endsection
