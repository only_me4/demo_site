@extends('layouts.app')

@section('content')
    <section style="background-color: #eee;">
        <div class="text-center container-sm py-5">
            <h4 class="mt-4 mb-5"><strong>Products:</strong></h4>

            @forelse($products->chunk(3) as $threeProducts)
                <div class="row">
                    @foreach($threeProducts as $product)
                    <div class="col-lg-4 col-md-12 mb-4">
                        <div class="card">
                            <div class="bg-image hover-zoom ripple ripple-surface ripple-surface-light"
                                 data-mdb-ripple-color="light">
                                <a href="{{asset('products/'.$product->id)}}" > <img src="{{asset('images/'.$product->image_path)}}"
                                                                                     class="w-100" style="height: 240px;object-fit: cover;" /> </a>
                                <a href="#!">
                                    <div class="mask">
                                        <div class="d-flex justify-content-start align-items-end h-100">
                                        </div>
                                    </div>
                                    <div class="hover-overlay">
                                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                                    </div>
                                </a>
                            </div>
                            <div class="card-body">
                                <a href="{{asset('products/'.$product->id)}}" class="text-reset text-decoration-none">
                                    <h5 class="card-title mb-3">{{$product->name}}</h5>
                                </a>
                                @if($product->categories->first()!==null)
                                    <a href="{{asset('categories/'.$product->categories->first()->id)}}" class="text-reset">
                                        <p>{{$product->categories->first()->name}}</p>
                                    </a>
                                @else
                                    <p>NULL</p>
                                @endif

                                <h6 class="mb-3">${{$product->price}}</h6>
                                <span>
                                    <button class="btn btn-primary font-weight-bold btn-sm" type = "submit" onclick="location.href='{{url("products/".$product->id."/edit")}}'">Edit</button>
                                <form style="display: inline;" action = '/products/{{$product->id}}' method = "POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger font-weight-bold btn-sm" type = "submit">Delete</button>
                                </form>
                                    </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @empty
                <p>There are no products</p>
            @endforelse
            <div>{{$products->links('pagination::bootstrap-5')}}</div>
        </div>
    </section>
@endsection
